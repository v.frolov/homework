'use strict'

const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');

const app = express();

app.set('port', 8080);
app.use(cors());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));

app.get('/ping', (req, res, next) => {
	res.send('pong');
	next(new Error());
});

app.use('/group', require('./controllers/groupsController'));
app.use('/room', require('./controllers/roomsController'));
app.use('/value', require('./controllers/valuesController'));

app.use((err, req, res, next) => {
	console.error(err.stack);
	return res.status(500).send(err);
});

let startServer = () => {
	app.listen(8080, () => {
		console.log('Express server started on port 8080');
	});
};
startServer();