'use strict'

class Value{
	constructor(){
		this.isNew = true;
		this.id = 0;
		this.name = '';
	}

	static findAll(connection, cb){
		
		var query = connection.query('select * from mes_values order by id_val');
		query.on('row', (row, res) => {
			res.addRow(row);
		});
		query.on('end', (res)=>{
			console.log (res.rows.length);
			cb(res.rows);
		});
	}

	static find(id, connection, cb){
		var query = connection.query('select * from mes_values where id_val = $1', [id]);
		query.on('row', (row, res) => {
			res.addRow(row);
		});
		query.on('end', (res)=>{
			console.log (res.rows.length);
			cb(res.rows);
		});
	}

	setData(id, name, connection, cb){
		var query = connection.query('update mes_values set name = $1 where id_val = $2', [name, id]);
		query.on('end', (res)=>{
			cb(res.rows.length);
		});
	}

	save(name, connection, cb){
		var query = connection.query('insert into mes_values (name) values($1) ', [name]);
		query.on('row', (row, res) => {
			res.addRow(row);
		});
		query.on('end', (res)=>{
		
			cb(res.rows.length);
		});
	}

	delete(id, connection, cb){
		var query = connection.query('delete from mes_values where id_val = ($1) ', [id]);
		query.on('end', (res)=>{
			cb(res.rows.length);
		});
	}
}

module.exports = Value;