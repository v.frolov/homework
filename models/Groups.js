'use strict'

class Group{
	constructor(){
		this.isNew = true;
		this.id = 0;
		this.name = '';
	}

	static findAll(connection, cb){
		
		var query = connection.query('select * from groups order by id_group');
		query.on('row', (row, res) => {
			res.addRow(row);
		});
		query.on('end', (res)=>{
			console.log (res.rows.length);
			cb(res.rows);
		});
	}

	static find(id, connection, cb){
		var query = connection.query('select * from groups where id_group = $1', [id]);
		query.on('row', (row, res) => {
			res.addRow(row);
		});
		query.on('end', (res)=>{
			console.log (res.rows.length);
			cb(res.rows);
		});
	}

	setData(id, name, connection, cb){
		var query = connection.query('update groups set name = $1 where id_group = $2', [name, id]);
		query.on('end', (res)=>{
			cb(res.rows.length);
		});
	}

	save(name, connection, cb){
		var query = connection.query('insert into groups (name) values($1) ', [name]);
		query.on('row', (row, res) => {
			res.addRow(row);
		});
		query.on('end', (res)=>{
		
			cb(res.rows.length);
		});
	}

	delete(id, connection, cb){
		var query = connection.query('delete from groups where id_group = ($1) ', [id]);
		query.on('end', (res)=>{
			cb(res.rows.length);
		});
	}
}

module.exports = Group;