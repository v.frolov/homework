'use strict'

const express = require('express');
const app = express();
const pg = require('pg');

var con = new pg.Client({
    user: 'postgres', //env var: PGUSER
    database: 'my_db', //env var: PGDATABASE
    password: '123', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5432, 
});
con.connect();

var Group = require('../models/Groups');

app.get('/(:group_id)?', (req, resp, next) => {
	let id = req.params.group_id;
	if(id) {
		Group.find(id, con, (groups) => resp.send(groups.map(group => group.name)));
	}
	else {
		Group.findAll(con, (groups) => resp.send(groups.map(group => group.name)));
	}
});

app.post('/', (req, res, next) => {
    if (!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let group = new Group();
    if(!req.body.name)
  	    return res.status(400).send('Empty data');
    console.log(req.body.name);
    group.save(req.body.name, con, (n) => res.sendStatus(201));
});

app.put('/:group_id', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.status(400).send('Empty data');
  }
  let group = new Group();
  let id = req.params.group_id;
  group.setData(id, req.body.name, con, (n) => res.send('ok'));
  
});

app.delete('/:group_id', (req, res, next) => {
	let id = req.params.group_id;
  	if (!id) {
    return res.sendStatus(400).send('Empty data');
  	}
  	let group = new Group();
  	group.delete(id, con, (n) => res.sendStatus(200).send(n));
});
module.exports = app;