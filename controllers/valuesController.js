'use strict'

const express = require('express');
const app = express();
const pg = require('pg');

var con = new pg.Client({
    user: 'postgres', //env var: PGUSER
    database: 'my_db', //env var: PGDATABASE
    password: '123', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5432, 
});
con.connect();

var Value = require('../models/Values');

app.get('/(:value_id)?', (req, resp, next) => {
	let id = req.params.value_id;
	if(id) {
		Value.find(id, con, (values) => resp.send(values.map(value => value.name)));
	}
	else {
		Value.findAll(con, (values) => resp.send(values.map(value => value.name)));
	}
});

app.post('/', (req, res, next) => {
    if (!Object.keys(req.body).length) {
        return res.status(400).send('Empty data');
    }

    let value = new Value();
    if(!req.body.name)
  	    return res.status(400).send('Empty data');
    console.log(req.body.name);
    value.save(req.body.name, con, (n) => res.sendStatus(201));
});

app.put('/:value_id', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.status(400).send('Empty data');
  }
  let value = new Value();
  let id = req.params.value_id;
  value.setData(id, req.body.name, con, (n) => res.send('ok'));
  
});

app.delete('/:value_id', (req, res, next) => {
	let id = req.params.value_id;
  	if (!id) {
    return res.sendStatus(400).send('Empty data');
  	}
  	let value = new Value();
  	value.delete(id, con, (n) => res.sendStatus(200).send(n));
});
module.exports = app;