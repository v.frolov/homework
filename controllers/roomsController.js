'use strict'

const express = require('express');
const app = express();
const pg = require('pg');

var con = new pg.Client({
    user: 'postgres', //env var: PGUSER
    database: 'my_db', //env var: PGDATABASE
    password: '123', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5432, 
});
con.connect();

var room = require('../models/Rooms');

app.get('/(:room_id)?', (req, resp, next) => {
	let id = req.params.room_id;
	if(id) {
		room.find(id, con, (rooms) => resp.send(rooms.map(room => room.name)));
	}
	else {
		room.findAll(con, (rooms) => resp.send(rooms.map(room => room.name)));
	}
});

app.post('/', (req, res, next) => {
    if (!Object.keys(req.body).length) {
        return res.sendStatus(400).send('Empty data');
    }
    if(!req.body.name)
        return res.sendStatus(400).send('Empty data');
    let room = new Room();
    console.log(req.body.name);
    room.save(req.body.name, con, (n) => res.sendStatus(201));
});

app.put('/:room_id', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.sendStatus(400).send('Empty data');
  }
  let room = new Room();
  let id = req.params.room_id;
  room.setData(id, req.body.name, con, (n) => res.send('ok'));
  
});

app.delete('/:room_id', (req, res, next) => {
	let id = req.params.room_id;
  	if (!id) {
    return res.sendStatus(400).send('Empty data');
  	}
  	let room = new Room();
  	room.delete(id, con, (n) => res.sendStatus(200).send(n));
});
module.exports = app;